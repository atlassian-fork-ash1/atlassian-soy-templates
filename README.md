# Contributing

All contributions need to have an associated issue in http://ecosystem.atlassian.net/browse/SOY.
Contributions made without an associated issue will not be accepted. If you are creating a branch
within this repository, please use issue/ISSUEKEY as the branch name. If you are using your own
fork you can name the branch whatever you want.

When your changes are ready you will need to raise a pull request against the target release branch.
This may just be `master` or `soy-MAJOR.MINOR.x` branch. The pull request must have at least the
project maintainer (Jason Hinch) as a reviewer.

If you require a build immediately, please create a milestone with the name of your product in the
version. e.g. 2.2.1-m1-confluence-1

Once your pull request has been approved you may merge it and perform a release. Do this in
consultation with the project maintainer to select the correct release or milestone version

# References

* Issues - https://ecosystem.atlassian.net/browse/SOY
* CI - https://engservices-bamboo.internal.atlassian.com/browse/SOY-MAIN

## CLI

This projects delivers a CLI for processing soy using the bundled Atlassian soy functions. It can be used to render soy,
or to compile to js.

The CLI requires JDK 1.8, as does the rest of the project.

The CLI accepts the following options:

    java -jar atlassian-soy-support.jar
        --type [render|js]
        --basedir basedir
        --glob glob
        --outdir outdir
        [--i18n i18nFile]
        [--outputExtension outputExtension]
        [--rootNamespace rootNamespace]
        [--dependencies dependencies]
        [--data data]
        [--functions function1.jar function2.jar...]

### Options

- `type` must be "render" to render the template or "js" to compile the template to js.
- `basedir` and glob are the directory and glob pattern containing the files to render or compile.
  - For the renderer, this will render one template per file, which is the path of the file relative to `basedir`.
For example, a file in `basedir/a/b.soy` will attempt to render the template `a.b` in the file `a/b.soy`.
  - For the compiler, this compiles all of the matches templates to javascript.
- `outdir` is the directory to output rendered / compiled files to. The files will be delivered matching the location of the
location of files under `basedir`.
- `i18nFile` is the glob for files containing i18n values represented by the `AJS.getText()` function. 
The glob searches under the value of `basedir` This is required
if the templates contain the `AJS.getText()` function.
- `outputExtension` is the extension given to the rendered files.
- `rootNamespace` (type=render only) is the root namespace of the templates to render.
- `dependencies` (type=render only) is a collection of locations of dependencies for the rendered files. This param
is in the format `dir1:glob1,dir2:glob2,...` where `dir` is the base dir of a dependency and `glob` is a glob matching files to find.
- `data` (type=render only) is a map of data to pass to the soy renderer, in the form `key1:value1,key2:value2,...`.

### Providing custom soy functions

Custom soy functions are loaded by a [ServiceLoader](http://docs.oracle.com/javase/7/docs/api/java/util/ServiceLoader.html). 
Your custom functions can be packaged into a jar with a META-INF/services/com.atlassian.soy.renderer.SoyFunction file enumerating 
each custom function's FQCN. Adding this jar to the classpath when invoking the CLI will ensure that they are included when 
rendering or compiling. 
