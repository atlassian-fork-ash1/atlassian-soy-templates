package com.atlassian.soy.renderer;

import com.atlassian.annotations.PublicSpi;

/**
 * Custom converter that allows one object to be substituted for another when converting a Java object into SoyData.
 * To register a converter, export it as an OSGi service under this interface. To consume a converter, mark your
 * class with the {@link CustomSoyDataMapper} annotation.
 *
 * @since 1.1
 */
@PublicSpi
public interface SoyDataMapper<I, O> {
    /**
     * The name of the mapper. Since concrete class information is lost when the mapper is turned into an OSGi service,
     * this is the only useful way to identify which mapper should be applied to which object. This name should
     * correspond to the value of the {@link CustomSoyDataMapper} annotation on the classes that want to be mapped.
     *
     * @return the name of the mapper.
     */
    public String getName();

    /**
     * Convert the object to be mapped into its custom mapping. The output could be some type that can be directly
     * mapped to SoyData (i.e. a primitive or simple collection), or it could be an Object that requires further
     * processing. There is no protection against infinite loops, so don't return another object of the same type!
     * <p>
     * If the mapper returns a {@code Callable}, the @{code Callable} will be used to perform the conversion as lazily
     * as possible.
     *
     * @param input the object to be mapped
     * @return the result of the mapping
     */
    public O convert(I input);
}
