package com.atlassian.soy.renderer;

import com.atlassian.annotations.PublicApi;

/**
 * @since 1.0
 */
@PublicApi
public class SoyException extends RuntimeException {
    public SoyException(String message) {
        super(message);
    }

    public SoyException(String message, Throwable cause) {
        super(message, cause);
    }

    public SoyException(Throwable cause) {
        super(cause);
    }
}
