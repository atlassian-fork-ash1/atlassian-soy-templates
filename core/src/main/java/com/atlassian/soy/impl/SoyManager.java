package com.atlassian.soy.impl;

import com.atlassian.soy.renderer.SoyException;

import java.util.Map;

public interface SoyManager {
    /**
     * Compile a soy file to JavaScript
     *
     * @param content  the content of the soy template
     * @param filePath the file path to the soy template
     * @return the compiled
     * @since 3.2
     */
    String compile(CharSequence content, String filePath);

    /**
     * Render a template from a module key into an appendable.
     *
     * @param appendable        the appendable to write to
     * @param completeModuleKey - a complete plugin module key containing the template resource
     * @param templateName      - a namespaced Soy template name
     * @param data              - a map of data to render the template with
     * @param injectedData      - a map of injected data to render the template with
     * @throws SoyException when an error occurs in rendering the template
     */
    void render(Appendable appendable, String completeModuleKey, String templateName,
                Map<String, Object> data, Map<String, Object> injectedData) throws SoyException;

    /**
     * Clear the caches associated with the module key. If {@code null}, clear all caches.
     *
     * @param completeModuleKey the module key
     */
    void clearCaches(String completeModuleKey);
}
