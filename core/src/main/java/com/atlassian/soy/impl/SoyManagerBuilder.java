package com.atlassian.soy.impl;

import com.atlassian.soy.impl.data.AtlassianSoyCustomValueConverter;
import com.atlassian.soy.impl.data.CachingJavaBeanAccessorResolver;
import com.atlassian.soy.impl.data.IntrospectorJavaBeanAccessorResolver;
import com.atlassian.soy.impl.data.JavaBeanAccessorResolver;
import com.atlassian.soy.impl.data.SoyDataMapperManager;
import com.atlassian.soy.impl.functions.ServiceLoaderSoyFunctionSupplier;
import com.atlassian.soy.impl.i18n.ResourceBundleI18nResolver;
import com.atlassian.soy.impl.i18n.WebContextJsLocaleResolver;
import com.atlassian.soy.impl.modules.DefaultGuiceModuleSupplier;
import com.atlassian.soy.impl.web.SimpleWebContextProvider;
import com.atlassian.soy.renderer.SoyDataMapper;
import com.atlassian.soy.spi.TemplateSetFactory;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.atlassian.soy.spi.modules.GuiceModuleSupplier;
import com.atlassian.soy.spi.web.WebContextProvider;

import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * A builder for a {@link DefaultSoyManager}
 *
 * @since 3.2
 */
public class SoyManagerBuilder {

    private I18nResolver i18nResolver;
    private JsLocaleResolver jsLocaleResolver;
    private GuiceModuleSupplier moduleSupplier;
    private Properties properties;
    private List<SoyDataMapper<?, ?>> soyDataMappers;
    private SoyFunctionSupplier soyFunctionSupplier;
    private TemplateSetFactory templateSetFactory;
    private WebContextProvider webContextProvider;

    public SoyManagerBuilder webContextProvider(WebContextProvider webContextProvider) {
        this.webContextProvider = webContextProvider;
        return this;
    }

    public boolean hasWebContextProvider() {
        return webContextProvider != null;
    }

    public SoyManagerBuilder templateSetFactory(TemplateSetFactory templateSetFactory) {
        this.templateSetFactory = templateSetFactory;
        return this;
    }

    public boolean hasTemplateSetFactory() {
        return templateSetFactory != null;
    }

    public SoyManagerBuilder localeResolver(JsLocaleResolver jsLocaleResolver) {
        this.jsLocaleResolver = jsLocaleResolver;
        return this;
    }

    public boolean hasLocaleResolver() {
        return jsLocaleResolver != null;
    }

    public SoyManagerBuilder i18nResolver(I18nResolver i18nResolver) {
        this.i18nResolver = i18nResolver;
        return this;
    }

    public boolean hasI18nResolver() {
        return i18nResolver != null;
    }

    public SoyManagerBuilder moduleSupplier(GuiceModuleSupplier moduleSupplier) {
        this.moduleSupplier = moduleSupplier;
        return this;
    }

    public boolean hasModuleSupplier() {
        return moduleSupplier != null;
    }

    public SoyManagerBuilder dataMappers(List<SoyDataMapper<?, ?>> soyDataMappers) {
        this.soyDataMappers = soyDataMappers;
        return this;
    }

    public SoyManagerBuilder dataMappers(SoyDataMapper<?, ?>... soyDataMappers) {
        return dataMappers(Arrays.asList(soyDataMappers));
    }

    public boolean hasDataMappers() {
        return soyDataMappers != null;
    }

    public SoyManagerBuilder functionSupplier(SoyFunctionSupplier soyFunctionSupplier) {
        this.soyFunctionSupplier = soyFunctionSupplier;
        return this;
    }

    public boolean hasFunctionSupplier() {
        return soyFunctionSupplier != null;
    }

    public SoyManagerBuilder properties(Properties properties) {
        this.properties = properties;
        return this;
    }

    public boolean hasProperties() {
        return properties != null;
    }

    public DefaultSoyManager build() {
        SoyDataMapperManager soyDataMapperManager = hasDataMappers() ?
                new SoyDataMapperManager(soyDataMappers) :
                new SoyDataMapperManager();

        if (!hasFunctionSupplier()) {
            functionSupplier(new ServiceLoaderSoyFunctionSupplier());
        }

        JavaBeanAccessorResolver javaBeanAccessorResolver =
                new CachingJavaBeanAccessorResolver(new IntrospectorJavaBeanAccessorResolver());

        if (!hasModuleSupplier()) {
            if (!hasWebContextProvider()) {
                webContextProvider(new SimpleWebContextProvider());
            }
            if (!hasI18nResolver()) {
                i18nResolver(new ResourceBundleI18nResolver(webContextProvider));
            }
            if (!hasLocaleResolver()) {
                localeResolver(new WebContextJsLocaleResolver(webContextProvider));
            }
            if (!hasProperties()) {
                properties(System.getProperties());
            }

            moduleSupplier(new DefaultGuiceModuleSupplier(
                    new AtlassianSoyCustomValueConverter(javaBeanAccessorResolver, soyDataMapperManager),
                    i18nResolver,
                    jsLocaleResolver,
                    properties,
                    soyFunctionSupplier,
                    webContextProvider
            ));
        }

        if (!hasTemplateSetFactory()) {
            templateSetFactory(new SimpleTemplateSetFactory(Collections.<URL>emptySet()));
        }

        return new DefaultSoyManager(moduleSupplier, javaBeanAccessorResolver, templateSetFactory);
    }

}
