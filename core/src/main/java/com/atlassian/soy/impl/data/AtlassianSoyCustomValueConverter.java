package com.atlassian.soy.impl.data;

import com.atlassian.soy.renderer.CustomSoyDataMapper;
import com.atlassian.soy.renderer.SanitizationType;
import com.atlassian.soy.renderer.SanitizedString;
import com.atlassian.soy.renderer.SoyDataMapper;
import com.atlassian.soy.renderer.SoyException;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.base.Throwables;
import com.google.template.soy.data.SanitizedContent;
import com.google.template.soy.data.SoyCustomValueConverter;
import com.google.template.soy.data.SoyValueConverter;
import com.google.template.soy.data.SoyValueProvider;
import com.google.template.soy.data.UnsafeSanitizedContentOrdainer;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static com.google.common.base.Preconditions.checkNotNull;

public class AtlassianSoyCustomValueConverter implements SoyCustomValueConverter {

    private final JavaBeanAccessorResolver accessorResolver;
    private final SoyDataMapperManager soyDataMapperManager;


    public AtlassianSoyCustomValueConverter(JavaBeanAccessorResolver accessorResolver, SoyDataMapperManager soyDataMapperManager) {
        this.accessorResolver = accessorResolver;
        this.soyDataMapperManager = soyDataMapperManager;
    }

    @Override
    public SoyValueProvider convert(SoyValueConverter converter, Object value) {
        if (value instanceof Enum) {
            return new EnumSoyValue((Enum<?>) value);
        }
        if (value instanceof SanitizedString) {
            SanitizedString sanitizedString = (SanitizedString) value;
            return UnsafeSanitizedContentOrdainer.ordainAsSafe(
                    sanitizedString.getValue(),
                    toContentKind(sanitizedString.getType())
            );
        }
        if (value.getClass().isArray()) {
            int length = Array.getLength(value);
            List<Object> list = new ArrayList<>(length);
            for (int i = 0; i < length; i++) {
                list.add(Array.get(value, i));
            }
            return converter.convert(list);
        }

        SoyDataMapper mapper = findMapper(value);
        if (mapper != null) {
            //noinspection unchecked
            Object newValue = mapper.convert(value);
            if (newValue != value) {
                return converter.convert(newValue);
            }
        }

        return new JavaBeanSoyDict(accessorResolver, converter, toSupplier(value));
    }

    private Supplier<?> toSupplier(Object value) {
        if (value instanceof Supplier) {
            return (Supplier<?>) value;
        }
        if (value instanceof Callable) {
            return new CallableSupplier((Callable<?>) value);
        }

        return Suppliers.ofInstance(value);
    }

    private <T> SoyDataMapper<T, ?> findMapper(T value) {
        CustomSoyDataMapper mapperConfig = value.getClass().getAnnotation(CustomSoyDataMapper.class);
        if (mapperConfig == null) {
            return null;
        }

        String mapperName = mapperConfig.value();
        SoyDataMapper<T, ?> mapper = soyDataMapperManager.getMapper(mapperName);
        if (mapper == null) {
            throw new SoyException("Could not find custom mapper " + mapperName + " for class " + value.getClass());
        }

        return mapper;
    }

    private static SanitizedContent.ContentKind toContentKind(SanitizationType type) {
        switch (type) {
            case CSS:
                return SanitizedContent.ContentKind.CSS;
            case JS:
                return SanitizedContent.ContentKind.JS;
            case JS_STRING:
                return SanitizedContent.ContentKind.JS_STR_CHARS;
            case HTML:
                return SanitizedContent.ContentKind.HTML;
            case HTML_ATTRIBUTE:
                return SanitizedContent.ContentKind.ATTRIBUTES;
            case TEXT:
                return SanitizedContent.ContentKind.TEXT;
            case URI:
                return SanitizedContent.ContentKind.URI;
            default:
                throw new UnsupportedOperationException("Unsupported type " + type);
        }
    }

    private static class CallableSupplier implements Supplier<Object> {

        private final Callable<?> callable;

        private CallableSupplier(Callable<?> callable) {
            this.callable = checkNotNull(callable, "callable");
        }

        @Override
        public Object get() {
            try {
                return callable.call();
            } catch (Exception e) {
                throw Throwables.propagate(e);
            }
        }
    }


}
