package com.atlassian.soy.impl.data;

import com.google.template.soy.data.SoyAbstractValue;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.restricted.StringData;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

public class EnumSoyValue extends SoyAbstractValue {

    private final Enum<?> value;

    public EnumSoyValue(final Enum<?> value) {
        this.value = checkNotNull(value, "value");
    }

    @Override
    public boolean equals(@Nonnull SoyValue other) {
        return (other instanceof EnumSoyValue && value.equals(((EnumSoyValue) other).value)) ||
                (other instanceof StringData && value.name().equals(other.stringValue()));
    }

    @Override
    public boolean coerceToBoolean() {
        return true;
    }

    @Override
    public String coerceToString() {
        return value.name();
    }

    public Enum<?> getValue() {
        return value;
    }

}
