package com.atlassian.soy.impl.data;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.template.soy.data.SoyAbstractMap;
import com.google.template.soy.data.SoyDataException;
import com.google.template.soy.data.SoyDict;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.SoyValueConverter;
import com.google.template.soy.data.SoyValueProvider;
import com.google.template.soy.data.restricted.StringData;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class JavaBeanSoyDict extends SoyAbstractMap implements SoyDict {

    private final JavaBeanAccessorResolver accessorResolver;
    private final SoyValueConverter converter;
    private final Supplier<?> supplier;

    private volatile Map<String, Method> accessors;

    public JavaBeanSoyDict(JavaBeanAccessorResolver accessorResolver, SoyValueConverter converter, Supplier<?> supplier) {
        this.accessorResolver = checkNotNull(accessorResolver, "accessorResolver");
        this.converter = checkNotNull(converter, "converter");
        this.supplier = Suppliers.memoize(checkNotNull(supplier, "supplier"));
    }

    @Nonnull
    @Override
    public Map<String, ? extends SoyValueProvider> asJavaStringMap() {
        return Maps.transformValues(getAccessors(), new Function<Method, SoyValueProvider>() {
            @Override
            public SoyValueProvider apply(Method accessorMethod) {
                return new AccessorSoyValueProvider(converter, getDelegate(), accessorMethod);
            }
        });
    }

    @Nonnull
    @Override
    public Map<String, ? extends SoyValue> asResolvedJavaStringMap() {
        return Maps.transformValues(asJavaStringMap(), new Function<SoyValueProvider, SoyValue>() {
            @Override
            public SoyValue apply(SoyValueProvider soyValueProvider) {
                return soyValueProvider.resolve();
            }
        });
    }

    public Object getDelegate() {
        return supplier.get();
    }

    @Override
    public int getItemCnt() {
        return getAccessors().size();
    }

    @Nonnull
    @Override
    public Iterable<? extends SoyValue> getItemKeys() {
        return Iterables.transform(getAccessors().keySet(), new Function<String, SoyValue>() {
            @Override
            public SoyValue apply(String value) {
                return StringData.forValue(value);
            }
        });
    }

    @Override
    public boolean hasItem(@Nonnull SoyValue key) {
        return getAccessors().containsKey(stringValue(key));
    }

    @Override
    public SoyValueProvider getItemProvider(@Nonnull SoyValue key) {
        return getFieldProvider(stringValue(key));
    }

    @Override
    public boolean hasField(@Nonnull String name) {
        return getAccessors().containsKey(name);
    }

    @Override
    public SoyValue getField(@Nonnull String name) {
        SoyValueProvider fieldProvider = getFieldProvider(name);
        return fieldProvider == null ? null : fieldProvider.resolve();
    }

    @Override
    public SoyValueProvider getFieldProvider(@Nonnull String name) {
        Method accessorMethod = getAccessors().get(name);
        return accessorMethod == null ? null : new AccessorSoyValueProvider(converter, getDelegate(), accessorMethod);
    }

    private Map<String, Method> getAccessors() {
        Map<String, Method> accessors = this.accessors;
        if (accessors == null) {
            synchronized (this) {
                accessors = this.accessors;
                if (accessors == null) {
                    accessors = this.accessors = accessorResolver.resolveAccessors(getDelegate().getClass());
                }

            }
        }
        return accessors;
    }

    private String stringValue(SoyValue key) {
        try {
            return ((StringData) key).getValue();
        } catch (ClassCastException e) {
            throw new SoyDataException(
                    "SoyDict accessed with non-string key (got key type " + key.getClass().getName() + ").");
        }
    }

}
