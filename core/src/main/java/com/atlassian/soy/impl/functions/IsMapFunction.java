package com.atlassian.soy.impl.functions;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Singleton;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyDict;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.restricted.BooleanData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.shared.restricted.SoyJavaFunction;

import java.util.List;
import java.util.Set;

/**
 * Soy util function for changing output based on whether a param is a map.
 *
 * @since 2.3
 */
@Singleton
public class IsMapFunction implements SoyJsSrcFunction, SoyJavaFunction {
    public static final String FUNCTION_NAME = "isMap";

    @Override
    public String getName() {
        return FUNCTION_NAME;
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return ImmutableSet.of(1);
    }

    @Override
    public JsExpr computeForJsSrc(List<JsExpr> jsExprs) {
        return new JsExpr("Object.prototype.toString.call(" + jsExprs.get(0).getText() + ") === '[object Object]'", Integer.MAX_VALUE);
    }

    @Override
    public SoyData computeForJava(List<SoyValue> soyDatas) {
        return BooleanData.forValue(soyDatas.get(0) instanceof SoyDict);
    }
}
