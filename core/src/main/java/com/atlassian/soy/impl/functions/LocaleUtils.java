package com.atlassian.soy.impl.functions;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

/**
 * This is a copy of com.atlassian.webresource.plugin.i18n.LocaleUtils from
 * com.atlassian.plugins.atlassian-plugins-webresource-plugin. I'm copying this in here rather than exposing
 * on from webresources as it's essentially a hack for Java 1.6's lack of Locale serialization / deserialization.
 * If we exposed this on the webresources API, we'd have to support it after we move to Java 1.7+.
 * <p>
 * Utilities for serializing / deserializing locales.
 * <p>
 * Note that they are NOT designed to handle locales according to any standard eg BCP 47 - the only goal of this class
 * is its serialization and deserialization match = ie that that myLocale.equals(deserialize(serialize(myLocale)));
 * and vice versa.
 * <p>
 * This will be unnecessary once we move to Java 1.7, and should be replaced by
 * {@link java.util.Locale#forLanguageTag(String)} / {@link java.util.Locale#toLanguageTag()}.
 *
 * @since 2.8
 */
public class LocaleUtils {
    public static String serialize(Locale locale) {
        StringBuilder str = new StringBuilder(locale.getLanguage());

        String country = locale.getCountry();
        if (StringUtils.isBlank(country)) {
            return str.toString();
        }
        str.append("-").append(country);

        String variant = locale.getVariant();
        if (StringUtils.isBlank(variant)) {
            return str.toString();
        }
        str.append("-").append(variant);

        return str.toString();
    }

    public static Locale deserialize(String str) {
        String[] split = str.split("-");
        switch (split.length) {
            case 1:
                return new Locale(split[0]);
            case 2:
                return new Locale(split[0], split[1]);
            case 3:
                return new Locale(split[0], split[1], split[2]);
            default:
                throw new IllegalArgumentException("Cannot parse locale: " + str);
        }
    }
}
