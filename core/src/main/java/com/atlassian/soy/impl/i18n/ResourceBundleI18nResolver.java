package com.atlassian.soy.impl.i18n;

import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.web.WebContextProvider;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * An implementation of {@link I18nResolver} which delegates a {@link WebContextProvider} for the locale and manages
 * translations in a map
 *
 * @since 3.2
 */
public class ResourceBundleI18nResolver implements I18nResolver {

    private final Map<Locale, ResourceBundle> bundles;
    private final WebContextProvider webContextProvider;

    public ResourceBundleI18nResolver(final WebContextProvider webContextProvider) {
        this(webContextProvider, Collections.<Locale, ResourceBundle>emptyMap());
    }

    public ResourceBundleI18nResolver(
            final WebContextProvider webContextProvider,
            final Map<Locale, ResourceBundle> bundles) {
        this.bundles = bundles;
        this.webContextProvider = webContextProvider;
    }

    @Override
    public String getText(final String key) {
        return getText(webContextProvider.getLocale(), key, new Serializable[0]);
    }

    @Override
    public String getText(final String key, final Serializable... arguments) {
        return getText(webContextProvider.getLocale(), key, arguments);
    }

    @Override
    public String getText(final Locale locale, final String key) {
        return getText(locale, key, new Serializable[0]);
    }

    @Override
    public String getRawText(final Locale locale, final String key) {
        try {
            ResourceBundle resourceBundle = bundles.get(locale);
            if (resourceBundle != null) {
                return resourceBundle.getString(key);
            }
        } catch (MissingResourceException e) {
            // ignore
        }
        return key;
    }

    private String getText(final Locale locale, final String key, Serializable[] arguments) {
        String rawText = getRawText(locale, key);
        return key.equals(rawText) ? rawText : MessageFormat.format(rawText, arguments);
    }

}
