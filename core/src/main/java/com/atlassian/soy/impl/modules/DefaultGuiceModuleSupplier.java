package com.atlassian.soy.impl.modules;

import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.atlassian.soy.spi.modules.GuiceModuleSupplier;
import com.atlassian.soy.spi.web.WebContextProvider;
import com.google.common.collect.ImmutableList;
import com.google.inject.Module;
import com.google.template.soy.SoyModule;
import com.google.template.soy.data.SoyCustomValueConverter;
import com.google.template.soy.xliffmsgplugin.XliffMsgPluginModule;

import java.util.Properties;

/**
 * Default implementation of GuiceModuleSupplier. Provides the bare minimum needed to get soy working.
 *
 * @since 2.3
 */
public class DefaultGuiceModuleSupplier implements GuiceModuleSupplier {
    private final Iterable<Module> defaultModules;

    public DefaultGuiceModuleSupplier(SoyCustomValueConverter customValueConverter,
                                      I18nResolver i18nResolver,
                                      JsLocaleResolver jsLocaleResolver,
                                      Properties properties,
                                      SoyFunctionSupplier soyFunctionSupplier,
                                      WebContextProvider webContextProvider) {
        this.defaultModules = ImmutableList.<Module>builder()
                .add(new SoyModule())
                .add(new XliffMsgPluginModule())
                .add(new CustomValueModule())
                .add(new CoreFunctionsModule())
                .add(new CustomFunctionsModule(soyFunctionSupplier))
                .add(new BridgeModule(customValueConverter, i18nResolver, jsLocaleResolver, properties, webContextProvider))
                .build();
    }

    @Override
    public Iterable<Module> get() {
        return defaultModules;
    }
}
