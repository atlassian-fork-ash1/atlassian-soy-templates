package com.atlassian.soy.spi;

import java.net.URL;
import java.util.Set;

/**
 * SPI for resolving soy module keys into URLs for the soy files.
 *
 * @since 2.3
 */
public interface TemplateSetFactory {
    /**
     * Clears caches.
     */
    void clear();

    /**
     * Retrieve the template files applicable to a given a completeModuleKey.
     * The templates can be extracted from WebResources and SoyWebResources where supported (in the dependency
     * trees of the given plugin module), or they can simply be retrieved from the war's resources directory.
     */
    Set<URL> get(String completeModuleKey);
}
