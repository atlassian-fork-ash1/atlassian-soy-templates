package com.atlassian.soy.spi.i18n;

import java.io.Serializable;
import java.util.Locale;

/**
 * SPI for i18n resolution. Implementors should delegate to whatever the target stack supports
 *
 * @since 2.3
 */
public interface I18nResolver {
    /**
     * Does the same as {@link #getText(String, java.io.Serializable...)} however it is needed for velocity.
     *
     * @param key key for the i18ned message
     * @return I18ned string
     */
    String getText(String key);

    /**
     * Given a key and a list of arguments:-
     * 1) this method returns the i18ned text formatted with the arguments if the key can be resolved.
     * 2) otherwise, the key itself will be returned (after formatting).
     *
     * @param key       key for the i18ned message
     * @param arguments Optional list of arguments for the message.
     * @return I18ned string
     */
    String getText(String key, Serializable... arguments);

    /**
     * Returns the i18ned text formatted with the arguments if the key can be resolved. Otherwise, the key itself will
     * be returned (after formatting).
     *
     * @param locale locale for the i18ned message
     * @param key    key for the i18ned message
     * @return I18ned string
     */
    String getText(Locale locale, String key);

    /**
     * Retrieve the unformatted message text associated with this key.
     *
     * @param locale locale for the i18ned message
     * @param key    key for the i18ned message
     * @return the unformatted message text if key is found, otherwise returns key itself.
     */
    String getRawText(Locale locale, String key);
}
