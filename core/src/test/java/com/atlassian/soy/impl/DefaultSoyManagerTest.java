package com.atlassian.soy.impl;

import com.atlassian.soy.spi.TemplateSetFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultSoyManagerTest {
    private static final String MODULE_KEY = "yo";

    private DefaultSoyManager soyManager;

    @Before
    public void setUp() throws Exception {
        URL happyPathUri = getClass().getResource("/happy-path.soy");
        TemplateSetFactory templateSetFactory = new SimpleTemplateSetFactory(happyPathUri);

        soyManager = new SoyManagerBuilder()
                .templateSetFactory(templateSetFactory)
                .build();
    }

    @Test
    public void testRender() throws Exception {
        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.happyPath",
                Collections.<String, Object>singletonMap("name", "Mario"),
                Collections.<String, Object>emptyMap());

        assertEquals("<p>Hello, Mario</p>", sb.toString());
    }

    @Test
    public void testIsMapWhenNotAMap() throws Exception {
        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testIsMap",
                Collections.<String, Object>singletonMap("maybeAMap", "Cake"),
                Collections.<String, Object>emptyMap());

        assertEquals("<p></p>", sb.toString());
    }

    @Test
    public void testIsMapWhenItIsAMap() throws Exception {

        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testIsMap",
                Collections.<String, Object>singletonMap("maybeAMap", Collections.emptyMap()),
                Collections.<String, Object>emptyMap());

        assertEquals("<p>It's a map!</p>", sb.toString());
    }

    @Test
    public void testIsMapWhenItIsAPojo() throws Exception {
        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testIsMap",
                Collections.<String, Object>singletonMap("maybeAMap", new ISwearIAmAMap()),
                Collections.<String, Object>emptyMap());

        assertEquals("<p>It's a map!</p>", sb.toString());
    }

    @Test
    public void testIsListWhenItIsNotAList() throws Exception {
        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testIsList",
                Collections.<String, Object>singletonMap("maybeAList", "Cake"),
                Collections.<String, Object>emptyMap());

        assertEquals("<p></p>", sb.toString());
    }

    @Test
    public void testIsListWhenItIsListLike() throws Exception {
        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testIsList",
                Collections.<String, Object>singletonMap("maybeAList", Collections.emptySet()),
                Collections.<String, Object>emptyMap());

        assertEquals("<p>It's a list!</p>", sb.toString());
    }

    @Test
    public void testConcatWithMaps() throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("firstMap", Collections.singletonMap("blah", "blahblah"));
        data.put("secondMap", new ISwearIAmAMap());
        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testConcat", data, Collections.<String, Object>emptyMap());

        String actual = sb.toString();
        assertTrue(actual.contains("blah: blahblah"));
        assertTrue(actual.contains("honest: kinda"));
    }

    @Test
    public void testEnumCoercesToStringInExpression() throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("enumValue", Color.GREEN);
        data.put("stringValue", Color.GREEN.name());
        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testEnumExpression",
                data,
                Collections.<String, Object>emptyMap());

        assertEquals("<p>true</p>", sb.toString());

        data.put("enumValue", Color.BLUE);

        sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testEnumExpression",
                data,
                Collections.<String, Object>emptyMap());

        assertEquals("<p>false</p>", sb.toString());
    }

    @Test
    public void testStringMatchesEnumInSwitch() throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("enum", Color.GREEN);
        StringBuilder sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testEnumSwitch",
                data,
                Collections.<String, Object>emptyMap());

        assertEquals("<p>It's green!</p>", sb.toString());

        data.put("enum", Color.BLUE);

        sb = new StringBuilder();
        soyManager.render(sb, MODULE_KEY, "atlassian.soy.testEnumSwitch",
                data,
                Collections.<String, Object>emptyMap());

        assertEquals("<p>It's not green!</p>", sb.toString());
    }

    @Test
    public void testContentSecurityPolicySupport() throws Exception {
        // having
        StringBuilder cacheMiss = new StringBuilder();
        StringBuilder cacheHit = new StringBuilder();

        // when
        soyManager.render(cacheMiss, MODULE_KEY, "atlassian.soy.testCSP",
                Collections.emptyMap(),
                Collections.singletonMap("csp_nonce", "test_nonce"));
        soyManager.render(cacheHit, MODULE_KEY, "atlassian.soy.testCSP",
                Collections.emptyMap(),
                Collections.singletonMap("csp_nonce", "other_nonce"));

        // then
        assertEquals("<script nonce=\"test_nonce\">alert(2)</script>", cacheMiss.toString());
        assertEquals("<script nonce=\"other_nonce\">alert(2)</script>", cacheHit.toString());
    }

    public static class ISwearIAmAMap {

        private String honest = "kinda";

        public String getHonest() {
            return honest;
        }
    }


    public static enum Color {
        GREEN,
        BLUE
    }

}
