package com.atlassian.soy.impl;

import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.template.soy.SoyModule;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

public class SoyDependencyInjectorFactoryTest {
    private SoyDependencyInjectorFactory injectorFactory;

    @Before
    public void setUp() {
        Iterable<Module> defaultModules = ImmutableList.<Module>of(new SoyModule());
        injectorFactory = new SoyDependencyInjectorFactory(Suppliers.ofInstance(defaultModules));
    }

    @Test
    public void testInjectorIsCached() throws Exception {
        Injector defaultInjector = injectorFactory.get();
        assertSame(defaultInjector, injectorFactory.get());
    }

    @Test
    public void testClear() throws Exception {
        Injector defaultInjector = injectorFactory.get();
        injectorFactory.clear();
        assertNotSame(defaultInjector, injectorFactory.get());
    }

}
