package com.atlassian.soy.impl.data;

import com.atlassian.soy.renderer.CustomSoyDataMapper;
import com.atlassian.soy.renderer.SanitizationType;
import com.atlassian.soy.renderer.SanitizedString;
import com.atlassian.soy.renderer.SoyDataMapper;
import com.google.template.soy.data.SanitizedContent;
import com.google.template.soy.data.SoyList;
import com.google.template.soy.data.SoyValueHelper;
import com.google.template.soy.data.SoyValueProvider;
import com.google.template.soy.data.restricted.IntegerData;
import com.google.template.soy.data.restricted.StringData;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AtlassianSoyCustomValueConverterTest {

    private AtlassianSoyCustomValueConverter customValueConverter;
    private List<SoyDataMapper<?, ?>> mappers = new ArrayList<>();

    @Before
    public void setUp() throws Exception {

        customValueConverter = new AtlassianSoyCustomValueConverter(
                new IntrospectorJavaBeanAccessorResolver(),
                new SoyDataMapperManager(mappers)
        );
    }

    @Test
    public void testConvertEnum() throws Exception {
        EnumSoyValue soyValue = assertConvertsToType(MarioBros.MARIO, EnumSoyValue.class);
        assertEquals(MarioBros.MARIO, soyValue.getValue());
        assertEquals(MarioBros.MARIO.name(), soyValue.coerceToString());
        assertTrue(soyValue.coerceToBoolean());
    }

    @Test
    public void testConvertArbitraryObject() throws Exception {
        Batman batman = new Batman();
        JavaBeanSoyDict soyDict = assertConvertsToType(batman, JavaBeanSoyDict.class);
        assertEquals(batman, soyDict.getDelegate());
        assertEquals(StringData.forValue("Robin"), soyDict.getField("partner"));
        assertNull(soyDict.getField("parents"));
    }

    @Test
    public void testConvertArray() throws Exception {
        assertConvertsToType(new Object[0], SoyList.class);
        assertConvertsToType(new boolean[0], SoyList.class);
        assertConvertsToType(new byte[0], SoyList.class);
        assertConvertsToType(new short[0], SoyList.class);
        assertConvertsToType(new int[0], SoyList.class);
        assertConvertsToType(new long[0], SoyList.class);
        assertConvertsToType(new float[0], SoyList.class);
        assertConvertsToType(new double[0], SoyList.class);
    }

    @Test
    public void testConvertSanitizedString() throws Exception {
        String html = "<p>I'm so safe</p>";
        SanitizedContent content = assertConvertsToType(new SanitizedString(html), SanitizedContent.class);
        assertEquals(html, content.getContent());
        assertEquals(SanitizedContent.ContentKind.HTML, content.getContentKind());
    }

    @Test
    public void testConvertSanitizedStringRoundTrip() throws Exception {
        String value = "Some string";
        for (SanitizationType type : SanitizationType.values()) {
            SanitizedContent content = assertConvertsToType(new SanitizedString(value, type), SanitizedContent.class);
            Object converted = SoyValueUtils.fromSoyValue(content);
            assertThat(converted, instanceOf(SanitizedString.class));
            SanitizedString sanitizedString = (SanitizedString) converted;
            assertEquals(value, sanitizedString.getValue());
            assertEquals(type, sanitizedString.getType());
        }
    }

    @Test
    public void testCustomDataMapper() throws Exception {
        mappers.add(mockSoyDataMapper());

        IntegerData soyValue = assertConvertsToType(new TheAnswer(), IntegerData.class);
        assertEquals(42, soyValue.integerValue());
    }

    @Test
    public void testInheritedCustomDataMapper() throws Exception {
        mappers.add(mockSoyDataMapper());

        IntegerData soyValue = assertConvertsToType(new SubclassAnswer(), IntegerData.class);
        assertEquals(42, soyValue.integerValue());
    }

    private SoyDataMapper mockSoyDataMapper() {
        SoyDataMapper mapper = mock(SoyDataMapper.class);
        when(mapper.getName()).thenReturn("convert-me");
        when(mapper.convert(isA(TheAnswer.class))).thenReturn(42);
        return mapper;
    }

    @SuppressWarnings("unchecked")
    public <T extends SoyValueProvider> T assertConvertsToType(Object value, Class<T> type) {
        SoyValueProvider soyValueProvider = customValueConverter.convert(SoyValueHelper.UNCUSTOMIZED_INSTANCE, value);
        assertNotNull(soyValueProvider);
        assertThat(soyValueProvider, (Matcher) instanceOf(type));

        return (T) soyValueProvider;
    }

    public static enum MarioBros {
        MARIO,
        LUIGI
    }

    @CustomSoyDataMapper("convert-me")
    public static class TheAnswer {

    }

    //not annotated with CustomSoyDataMapper
    public static class SubclassAnswer extends TheAnswer {

    }

    public static class Batman {
        public String getPartner() {
            return "Robin";
        }
    }

}
