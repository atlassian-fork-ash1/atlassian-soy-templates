package com.atlassian.soy.impl.modules;

import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.atlassian.soy.spi.web.WebContextProvider;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.template.soy.SoyFileSet;
import com.google.template.soy.SoyModule;
import com.google.template.soy.data.SoyCustomValueConverter;
import com.google.template.soy.jssrc.SoyJsSrcOptions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the CustomFunctionsModule creates working functions from the pluginAccessor.
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomFunctionsModuleTest {
    private static final String FOO_TEMPLATE_WITH_FUNC_OUTPUT = "" +
            "// This file was automatically generated from foo.soy.\n" +
            "// Please don't edit this file by hand.\n" +
            "\n" +
            "/**\n" +
            " * @fileoverview Templates in namespace foo.\n" +
            " */\n" +
            "\n" +
            "if (typeof foo == 'undefined') { var foo = {}; }\n" +
            "\n" +
            "\n" +
            "foo.bar = function(opt_data, opt_ignored) {\n" +
            "  return '' + soy.$$escapeHtml(MYJS);\n" +
            "};\n" +
            "if (goog.DEBUG) {\n" +
            "  foo.bar.soyTemplateName = 'foo.bar';\n" +
            "}\n";

    @Mock
    private SoyFunctionSupplier soyFunctionSupplier;

    private CustomFunctionsModule customFunctionsModule;

    @Before
    public void setUp() {
        customFunctionsModule = new CustomFunctionsModule(soyFunctionSupplier);
    }

    @Test
    public void testItWorks() {
        when(soyFunctionSupplier.get()).thenReturn(Collections.<SoyFunction>singletonList(makeClientFunc("foo")));

        Injector injector = Guice.createInjector(customFunctionsModule, new SoyModule());
        SoyFileSet.Builder builder = injector.getInstance(SoyFileSet.Builder.class);
        assertCompiledSoy(builder);
    }

    @Test(expected = com.google.template.soy.base.SoySyntaxException.class)
    public void testMissingFunctionsBlowsUp() {
        when(soyFunctionSupplier.get()).thenReturn(Collections.<SoyFunction>emptyList());
        Injector injector = Guice.createInjector(customFunctionsModule, new SoyModule());
        SoyFileSet.Builder builder = injector.getInstance(SoyFileSet.Builder.class);
        assertCompiledSoy(builder);
    }

    @Test
    public void testFunctionsWithTheSameNameDoNotBlowUp() {
        when(soyFunctionSupplier.get()).thenReturn(Arrays.<SoyFunction>asList(makeClientFunc("foo"), makeClientFunc("foo")));
        Injector injector = Guice.createInjector(customFunctionsModule, new SoyModule());
        SoyFileSet.Builder builder = injector.getInstance(SoyFileSet.Builder.class);
        assertCompiledSoy(builder);
    }

    @Test
    public void testMixedFunctionDoesNotBlowUp() {
        when(soyFunctionSupplier.get()).thenReturn(Collections.singletonList(makeMixedFunc("foo")));
        Injector injector = Guice.createInjector(customFunctionsModule, new SoyModule());
        SoyFileSet.Builder builder = injector.getInstance(SoyFileSet.Builder.class);
        assertCompiledSoy(builder);
    }

    @Test
    public void testServerBeforeClientFunctionDoesNotBlowUp() {
        when(soyFunctionSupplier.get()).thenReturn(Arrays.asList(makeServerFunc("foo"), makeClientFunc("foo")));
        Injector injector = Guice.createInjector(customFunctionsModule, new SoyModule());
        SoyFileSet.Builder builder = injector.getInstance(SoyFileSet.Builder.class);
        assertCompiledSoy(builder);
    }

    @Test
    public void testFunctionsWithTheSameNameAsBuiltinDoNotBlowUp() {
        when(soyFunctionSupplier.get()).thenReturn(Arrays.<SoyFunction>asList(makeClientFunc("foo"), makeClientFunc("length")));
        Injector injector = Guice.createInjector(customFunctionsModule, new SoyModule());
        SoyFileSet.Builder builder = injector.getInstance(SoyFileSet.Builder.class);
        assertCompiledSoy(builder);
    }

    @Test
    public void testIsProvidedFunctionNameIsTrueForAllBuiltinFunctions() {
        Injector injector = Guice.createInjector(new SoyModule());
        // is there a better way to do this?
        SoyFunctionCollector collector = new SoyFunctionCollector();
        injector.injectMembers(collector);
        for (com.google.template.soy.shared.restricted.SoyFunction function : collector.values) {
            assertTrue(function.getName() + " should be identified as provided", CustomFunctionsModule.isProvidedFunctionName(function.getName()));
        }
    }

    @Test
    public void testBuiltInFunctionNamesMatchesBoundSoyFunctions() {
        Injector injector = Guice.createInjector(new SoyModule());
        // is there a better way to do this?
        SoyFunctionCollector collector = new SoyFunctionCollector();
        injector.injectMembers(collector);
        HashSet<String> registeredFunctionNames = new HashSet<String>(Collections2.transform(collector.values, new Function<com.google.template.soy.shared.restricted.SoyFunction, String>() {
            @Override
            public String apply(com.google.template.soy.shared.restricted.SoyFunction function) {
                return function.getName();
            }
        }));
        assertEquals(registeredFunctionNames, CustomFunctionsModule.BUILT_IN_FUNCTION_NAMES);
    }

    @Test
    public void testIsProvidedFunctionNameIsTrueForAllCoreFunctions() {
        Injector injector = Guice.createInjector(
                new BridgeModule(mock(
                        SoyCustomValueConverter.class),
                        mock(I18nResolver.class),
                        mock(JsLocaleResolver.class),
                        new Properties(),
                        mock(WebContextProvider.class)),
                new CoreFunctionsModule());
        // is there a better way to do this?
        SoyFunctionCollector collector = new SoyFunctionCollector();
        injector.injectMembers(collector);
        for (com.google.template.soy.shared.restricted.SoyFunction function : collector.values) {
            assertTrue(function.getName() + " should be identified as provided", CustomFunctionsModule.isProvidedFunctionName(function.getName()));
        }
    }

    private void assertCompiledSoy(SoyFileSet.Builder builder) {
        String out = compileSoy(builder);
        assertEquals(FOO_TEMPLATE_WITH_FUNC_OUTPUT, out);
    }

    private String compileSoy(SoyFileSet.Builder builder) {
        return builder.add("{namespace foo}\n/** */\n{template .bar}\n{foo()}\n{/template}\n", "foo.soy").build().compileToJsSrc(newOptions(), null).get(0);
    }

    private SoyClientFunction makeClientFunc(final String functionName) {
        return new SoyClientFunction() {
            @Override
            public String getName() {
                return functionName;
            }

            @Override
            public JsExpression generate(JsExpression... args) {
                return new JsExpression("MYJS");
            }

            @Override
            public Set<Integer> validArgSizes() {
                return ImmutableSet.of(0);
            }
        };
    }

    private SoyServerFunction<String> makeServerFunc(final String functionName) {
        return new SoyServerFunction<String>() {
            @Override
            public String getName() {
                return functionName;
            }

            @Override
            public String apply(Object... args) {
                return "MYSERVER";
            }

            @Override
            public Set<Integer> validArgSizes() {
                return ImmutableSet.of(0);
            }
        };
    }

    private SoyFunction makeMixedFunc(final String functionName) {
        class MixedFunction implements SoyClientFunction, SoyServerFunction<String> {
            @Override
            public String getName() {
                return functionName;
            }

            @Override
            public JsExpression generate(JsExpression... args) {
                return new JsExpression("MYJS");
            }

            @Override
            public String apply(Object... args) {
                return "MYSERVER";
            }

            @Override
            public Set<Integer> validArgSizes() {
                return ImmutableSet.of(0);
            }
        }

        return new MixedFunction();
    }

    private static SoyJsSrcOptions newOptions() {
        return new SoyJsSrcOptions();
    }

    static class SoyFunctionCollector {
        @Inject
        Set<com.google.template.soy.shared.restricted.SoyFunction> values;
    }

}
