plan(key: 'MAIN', name: 'Main') {
    project(key: 'SOY', name: 'Atlassian Soy')

    repository(name: 'Atlassian Soy')

    trigger(type: 'polling', strategy: 'periodically', frequency: '600') {
        repository(name: 'Atlassian Soy')
    }
    trigger(type: 'cron', description: 'Run once a month to prevent deletion',
            cronExpression: '0 0 0 1 * ?')

    notification(type: 'Failed Jobs and First Successful', recipient: 'committers')
    notification(type: 'All Builds Completed', recipient: 'hipchat',
                 apiKey: '${bamboo.atlassian.hipchat.apikey.password}',
                 notify: 'false', room: 'Atlassian Soy')

    // Maven release version variables
    variable(key: "maven.release.version", value: '')
    variable(key: "maven.next.version", value: '')

    branchMonitoring(enabled: 'true', notificationStrategy: 'INHERIT',
                     timeOfInactivityInDays: '30', remoteJiraBranchLinkingEnabled: 'true')
    
    stage(name: 'Test Stage') {
        job(key: 'TEST', name: 'Build and test') {
            requirement(key: 'os', condition: 'equals', value: 'Linux')
            artifactDefinition(name: 'Integration test logs', location: 'integration-tests/plugin/target',
                               pattern: '**/home/log/*', shared: 'false')

            task(type: 'checkout', description: 'Checkout Default Repository')
            task(type: 'maven3', goal: 'clean install', mavenExecutable: 'Maven 3.2', buildJdk: 'JDK 1.8',
                 hasTests: 'true', testDirectory: '**/target/surefire-reports/*.xml, **/target/**/surefire-reports/*.xml')
        }
    }

    maven_release_stages_simple(manual: 'true', binaryModule: 'plugin', repositoryName: 'Atlassian Soy',
                                versionVariable: 'maven.release.version', nextVersionVariable: 'maven.next.version',
                                releaseExtraArguments: '', releaseExtraProfiles: '', environmentVariables: '',
                                package: 'jar', mavenVersion: 'Maven 3.2', jdkVersion: 'JDK 1.8', tagPrefix: 'soy-templates-parent-')
}

plan(key: 'COVERAGE', name: 'Code coverage') {
    project(key: 'SOY', name: 'Atlassian Soy')

    repository(name: 'Atlassian Soy')

    trigger(type: 'polling', strategy: 'periodically', frequency: '600') {
        repository(name: 'Atlassian Soy')
    }
    trigger(type: 'cron', description: 'Run once a month to prevent deletion',
            cronExpression: '0 0 0 1 * ?')

    notification(type: 'Failed Jobs and First Successful', recipient: 'committers')

    stage(name: 'Coverage Stage') {
        job(key: 'JAVA', name: 'Build and test') {
            requirement(key: 'os', condition: 'equals', value: 'Linux')
            
            artifactDefinition(name: 'Clover Report', pattern: 'target/clover-report.tar', shared: 'false')

            miscellaneousConfiguration() {
                clover(type: 'auto', path: 'target/clover-report/clover.xml')
            }

            task(type: 'checkout', description: 'Checkout Default Repository')
            task(type: 'maven3', goal: 'verify -Pwith-clover', mavenExecutable: 'Maven 3.2', buildJdk: 'JDK 1.8',
                 hasTests: 'true', testDirectory: '**/target/surefire-reports/*.xml, **/target/**/surefire-reports/*.xml')
            task(type: 'script', description: 'Find Clover report and create a TAR archive',
                        scriptBody: '''
clover_xml_path="$(find . -name clover.xml)"
echo "Found Clover XML at $clover_xml_path"
clover_report_directory="$(dirname $clover_xml_path)"
mkdir -p target
cp -v -r $clover_report_directory target/clover-report
echo "Zipping report"
tar -cvf target/clover-report.tar target/clover-report
''')
        }
    }
}

plan(key: 'COMPILER', name: 'Closure Templates Fork') {
    project(key: 'SOY', name: 'Atlassian Soy')

    repository(name: 'Atlassian Soy (Closure Templates Fork)')

    trigger(type: 'polling', strategy: 'periodically', frequency: '600') {
        repository(name: 'Atlassian Soy')
    }
    trigger(type: 'cron', description: 'Run once a month to prevent deletion',
            cronExpression: '0 0 0 1 * ?')

    notification(type: 'Failed Jobs and First Successful', recipient: 'committers')

    // Maven release version variables
    variable(key: "maven.release.version", value: '')
    variable(key: "maven.next.version", value: '')

    branchMonitoring(enabled: 'true', notificationStrategy: 'INHERIT',
                     timeOfInactivityInDays: '30', remoteJiraBranchLinkingEnabled: 'true')
    
    stage(name: 'Test Stage') {
        job(key: 'TEST', name: 'Build and test') {
            requirement(key: 'os', condition: 'equals', value: 'Linux')

            task(type: 'checkout', description: 'Checkout Default Repository')
            task(type: 'maven3', goal: 'clean install', mavenExecutable: 'Maven 3.2', buildJdk: 'JDK 1.8',
                 hasTests: 'false')
        }
    }

    maven_release_stages_simple(manual: 'true', binaryModule: '.', repositoryName: 'Atlassian Soy (Closure Templates Fork)',
                                versionVariable: 'maven.release.version', nextVersionVariable: 'maven.next.version',
                                releaseExtraArguments: '', releaseExtraProfiles: '', environmentVariables: '',
                                package: 'jar', mavenVersion: 'Maven 3.2', jdkVersion: 'JDK 1.8', tagPrefix: 'soycompiler-')
}

