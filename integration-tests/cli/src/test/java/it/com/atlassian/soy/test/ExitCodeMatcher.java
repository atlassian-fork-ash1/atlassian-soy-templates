package it.com.atlassian.soy.test;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

public class ExitCodeMatcher extends FeatureMatcher<ProcessResult, Integer> {
    public ExitCodeMatcher(Matcher<? super Integer> subMatcher) {
        super(subMatcher, "exit code matches", "exit code");
    }

    @Override
    protected Integer featureValueOf(ProcessResult actual) {
        return actual.getExitCode();
    }

    public static ExitCodeMatcher hasExitCode(Matcher<? super Integer> subMatcher) {
        return new ExitCodeMatcher(subMatcher);
    }
}
