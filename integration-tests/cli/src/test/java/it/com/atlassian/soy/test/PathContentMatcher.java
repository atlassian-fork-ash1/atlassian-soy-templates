package it.com.atlassian.soy.test;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.file.Path;

public class PathContentMatcher extends FeatureMatcher<Path, String> {
    public PathContentMatcher(Matcher<? super String> subMatcher) {
        super(subMatcher, "path with content", "content");
    }

    @Override
    protected String featureValueOf(Path actual) {
        StringBuilder sb = new StringBuilder();
        CharBuffer buf = CharBuffer.allocate(1024);

        try {
            Reader in = new InputStreamReader(new FileInputStream(actual.toFile()));
            try {
                int i;
                while ((i = in.read(buf)) != -1) {
                    buf.flip();
                    sb.append(buf, 0, i);
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    public static PathContentMatcher hasContent(Matcher<? super String> subMatcher) {
        return new PathContentMatcher(subMatcher);
    }


}
