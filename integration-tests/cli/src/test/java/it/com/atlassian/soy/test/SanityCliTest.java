package it.com.atlassian.soy.test;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static it.com.atlassian.soy.test.ExitCodeMatcher.hasExitCode;
import static it.com.atlassian.soy.test.PathContentMatcher.hasContent;
import static it.com.atlassian.soy.test.StdOutMatcher.hasOutput;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeFalse;

public class SanityCliTest {

    private static Path soyCliPath;
    private static Path soyTestJarPath;
    private static Path testSoyDir;
    private static ExecutorService executor;

    @Rule
    public TemporaryFolder tmpDir = new TemporaryFolder();

    @BeforeClass
    public static void beforeClass() throws Exception {
        soyCliPath = Paths.get(System.getProperty("soy.cli.jar.path"));
        soyTestJarPath = Paths.get(System.getProperty("soy.test.jar.path"));
        testSoyDir = Paths.get(System.getProperty("soy.test.directory"));
        executor = Executors.newSingleThreadExecutor();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        executor.shutdown();
    }

    @Test
    public void testSanityJs() throws Exception {
        //Main.createClassLoader's URI operations don't work on Windows
        assumeFalse(SystemUtils.IS_OS_WINDOWS);

        ProcessResult result = execCli(
                "--type", "js",
                "--functions", soyTestJarPath.toString(),
                "--basedir", testSoyDir.toString(),
                "--outdir", tmpDir.getRoot().getAbsolutePath(),
                "--glob", "**/*.soy");
        assertThat(result, allOf(hasExitCode(equalTo(0)), hasOutput(containsString("sanity.js"))));
        assertThat(tmpDir.getRoot().toPath().resolve("view/sanity.js"), hasContent(containsString("Slim Shady")));
    }

    private ProcessResult execCli(String... args) throws IOException, InterruptedException, ExecutionException {
        List<String> command = new ArrayList<String>();
        command.add(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java");
        String jvmargs = System.getProperty("soy.cli.jvmargs");
        if (StringUtils.isNotBlank(jvmargs)) {
            command.add(jvmargs);
        }
        command.addAll(Arrays.asList("-jar", soyCliPath.toString()));
        command.addAll(Arrays.asList(args));
        System.out.println("Executing: " + StringUtils.join(command, ' '));
        Process process = new ProcessBuilder()
                .command(command)
                .redirectErrorStream(true)
                .start();
        List<Future<Object>> futures = executor.invokeAll(Arrays.asList(
                new ExitCodePump(process), new StdOutPump(process)));
        return new ProcessResult(
                (Integer) futures.get(0).get(),
                (String) futures.get(1).get()
        );
    }

    public static class StdOutPump implements Callable<Object> {
        private final Process process;

        public StdOutPump(Process process) {
            this.process = process;
        }

        @Override
        public String call() throws IOException {
            CharBuffer buf = CharBuffer.allocate(1024);
            StringBuilder out = new StringBuilder();
            try (Reader in = new InputStreamReader(process.getInputStream(), Charset.forName("UTF-8"))) {
                while (true) {
                    final int n = in.read(buf);
                    if (n == -1) {
                        break;
                    }
                    buf.flip();
                    out.append(buf, 0, n);
                }
            }
            return out.toString();
        }
    }

    public static class ExitCodePump implements Callable<Object> {
        private final Process process;

        public ExitCodePump(Process process) {
            this.process = process;
        }

        @Override
        public Integer call() throws InterruptedException {
            return process.waitFor();
        }
    }


}
