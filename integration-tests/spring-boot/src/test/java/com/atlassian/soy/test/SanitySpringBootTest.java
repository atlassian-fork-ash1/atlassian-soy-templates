package com.atlassian.soy.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class,
        properties = "server.servlet.context-path:" + SanitySpringBootTest.CONTEXT_PATH,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SanitySpringBootTest {

    static final String CONTEXT_PATH = "/soy-test";

    @Value("${local.server.port}")
    private int port;

    @Test
    public void testSanityPage() {
        ResponseEntity<String> entity = new TestRestTemplate().getForEntity("http://localhost:" + this.port + CONTEXT_PATH + "/sanity", String.class);
        assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(entity.getBody(), equalTo(
                "<!DOCTYPE html>" +
                        "<html lang=\"en\">" +
                        "<head>" +
                        "<meta charset=\"utf-8\" />" +
                        "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EDGE\">" +
                        "<title>You&#39;re on " + CONTEXT_PATH + "</title>" +
                        "</head>" +
                        "<body>Hello, Slim Shady!</body>" +
                        "</html>"
        ));
    }

}
