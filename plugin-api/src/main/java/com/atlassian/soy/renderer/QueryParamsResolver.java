package com.atlassian.soy.renderer;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.webresource.QueryParams;
import com.google.common.base.Supplier;

import javax.annotation.Nonnull;

/**
 * Resolve the {@link QueryParams} for the current web resource request. This can be used to facilitate
 * state specific {@link SoyClientFunction soy client functions}.
 *
 * @since 2.4
 */
@PublicApi
public interface QueryParamsResolver extends Supplier<QueryParams> {

    /**
     * @return the query params associated with the current web resource request
     * @throws java.lang.IllegalThreadStateException when this method is called outside of the context of a web resource request
     */
    @Nonnull
    QueryParams get();

}
