package com.atlassian.soy.impl.functions;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Collections;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * helpUrl('') soy function.
 * <p>
 * Populates soy template with help url at soy compilation time therefore assumption is that urls won't change at
 * application runtime
 *
 * @since 2.9
 */
@TenantAware(TenancyScope.TENANTLESS)
public class HelpUrlFunction implements SoyClientFunction, SoyServerFunction<String> {
    private static final Set<Integer> ARGS_SIZES = Collections.singleton(1);
    private static final Pattern SOY_STRING_PATTERN = Pattern.compile("^'(.*)'$");

    private final HelpPathResolver helpPathResolver;

    public HelpUrlFunction(HelpPathResolver helpPathResolver) {
        this.helpPathResolver = checkNotNull(helpPathResolver);
    }

    @Override
    public String getName() {
        return "helpUrl";
    }

    @Override
    public Set<Integer> validArgSizes() {
        return ARGS_SIZES;
    }

    @Override
    public JsExpression generate(JsExpression... args) {
        Matcher matcher = SOY_STRING_PATTERN.matcher(args[0].getText());
        checkArgument(matcher.matches(), "The help key name should be a string literal");

        String helpUrl = getHelpUrl(matcher.group(1));
        return new JsExpression('"' + StringEscapeUtils.escapeEcmaScript(helpUrl) + '"');
    }


    @Override
    public String apply(Object... args) {
        String helpKey = (String) args[0];
        return getHelpUrl(helpKey);
    }

    private String getHelpUrl(String helpKey) {
        return helpPathResolver.getHelpPath(helpKey).getUrl();
    }
}
