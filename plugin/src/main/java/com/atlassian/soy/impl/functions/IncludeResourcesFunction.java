package com.atlassian.soy.impl.functions;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.inject.Singleton;
import com.google.template.soy.data.SanitizedContent;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.UnsafeSanitizedContentOrdainer;
import com.google.template.soy.shared.restricted.SoyJavaFunction;

import javax.inject.Inject;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Singleton
public class IncludeResourcesFunction implements SoyJavaFunction {

    private static final Set<Integer> ARGS_SIZE = Collections.singleton(0);

    private final WebResourceManager webResourceManager;

    @Inject
    public IncludeResourcesFunction(WebResourceManager webResourceManager) {
        this.webResourceManager = webResourceManager;
    }

    @Override
    public SoyData computeForJava(List<SoyValue> args) {
        StringWriter writer = new StringWriter();
        webResourceManager.includeResources(writer, UrlMode.AUTO);
        return UnsafeSanitizedContentOrdainer.ordainAsSafe(writer.toString(), SanitizedContent.ContentKind.HTML);
    }

    @Override
    public String getName() {
        return "webResourceManager_includeResources";
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return ARGS_SIZE;
    }

}
