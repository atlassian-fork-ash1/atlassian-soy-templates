package com.atlassian.soy.impl.functions;

import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;
import com.atlassian.webresource.api.prebake.Dimensions;

public interface UrlEncodingSoyFunctionSupplier extends SoyFunctionSupplier, DimensionAwareTransformerUrlBuilder {

    Dimensions computeDimensions();

}
