package com.atlassian.soy.impl.i18n;

import com.atlassian.soy.impl.functions.LocaleUtils;
import com.atlassian.soy.renderer.QueryParamsResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

/**
 * JsLocaleResolver that reads from query params.
 *
 * @since 2.8
 */
public class QueryParamsJsLocaleResolver implements JsLocaleResolver {
    // This is intentionally the same query key as webresources' JsI18nTransformer. If they collide, they will always
    // be the same value.
    public static final String QUERY_KEY = "locale";

    private final QueryParamsResolver queryParamsResolver;

    public QueryParamsJsLocaleResolver(QueryParamsResolver queryParamsResolver) {
        this.queryParamsResolver = queryParamsResolver;
    }

    @Override
    public Locale getLocale() {
        String localeKey = queryParamsResolver.get().get(QUERY_KEY);
        if (!StringUtils.isBlank(localeKey)) {
            return LocaleUtils.deserialize(localeKey);
        } else {
            return Locale.US;
        }
    }
}
