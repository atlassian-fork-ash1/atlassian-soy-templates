package com.atlassian.soy.impl.webresource;

import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.soy.impl.functions.LocaleUtils;
import com.atlassian.soy.impl.functions.UrlEncodingSoyFunctionSupplier;
import com.atlassian.soy.impl.i18n.QueryParamsJsLocaleResolver;
import com.atlassian.soy.spi.web.WebContextProvider;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;

/**
 * @since 2.4
 */
public class SoyTransformerUrlBuilder implements DimensionAwareTransformerUrlBuilder {
    private final UrlEncodingSoyFunctionSupplier soyFunctionSupplier;
    private final WebContextProvider webContextProvider;

    public SoyTransformerUrlBuilder(UrlEncodingSoyFunctionSupplier soyFunctionSupplier, WebContextProvider webContextProvider) {
        this.soyFunctionSupplier = soyFunctionSupplier;
        this.webContextProvider = webContextProvider;
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {
        soyFunctionSupplier.addToUrl(urlBuilder);
        urlBuilder.addToQueryString(QueryParamsJsLocaleResolver.QUERY_KEY, LocaleUtils.serialize(webContextProvider.getLocale()));
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder, Coordinate coordinate) {
        String locale = coordinate.get(QueryParamsJsLocaleResolver.QUERY_KEY);
        soyFunctionSupplier.addToUrl(urlBuilder, coordinate);
        urlBuilder.addToQueryString(QueryParamsJsLocaleResolver.QUERY_KEY, locale);
    }
}
