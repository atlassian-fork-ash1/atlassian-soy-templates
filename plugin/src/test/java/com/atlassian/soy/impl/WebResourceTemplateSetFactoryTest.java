package com.atlassian.soy.impl;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.util.concurrent.UncheckedExecutionException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class WebResourceTemplateSetFactoryTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Mock
    private WebResourceTemplateSetFactory factory;
    @Mock
    private PluginAccessor pluginAccessor;

    private ServletContextFactory servletContextFactory;

    private Map<String, WebResourceModuleDescriptor> moduleDescriptors = new HashMap<>();
    private Map<String, Plugin> plugins = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        when(pluginAccessor.getEnabledPluginModule(anyString())).thenAnswer(new Answer<WebResourceModuleDescriptor>() {
            @Override
            public WebResourceModuleDescriptor answer(InvocationOnMock invocationOnMock) throws Throwable {
                //noinspection SuspiciousMethodCalls
                return moduleDescriptors.get(invocationOnMock.getArguments()[0]);
            }
        });
        factory = new WebResourceTemplateSetFactory(pluginAccessor, servletContextFactory);
    }

    private Plugin getOrInitPlugin(final String pluginKey) {
        Plugin plugin = plugins.get(pluginKey);
        if (plugin == null) {
            plugin = mock(Plugin.class);
            when(plugin.getKey()).thenReturn(pluginKey);
            when(plugin.getResource(anyString())).thenAnswer(new Answer<URL>() {
                @Override
                public URL answer(InvocationOnMock invocationOnMock) throws Throwable {
                    String path = (String) invocationOnMock.getArguments()[0];
                    if (pluginKey.isEmpty()) {
                        return new File(path).toURI().toURL();
                    }

                    return new URI("file", pluginKey, path, null).toURL();
                }
            });
            plugins.put(pluginKey, plugin);
        }
        return plugin;
    }

    private void addModule(String pluginKey, String moduleKey, List<String> resourceLocations, List<String> dependencies) {
        Plugin plugin = getOrInitPlugin(pluginKey);
        WebResourceModuleDescriptor descriptor = mock(WebResourceModuleDescriptor.class);
        when(descriptor.getKey()).thenReturn(moduleKey);
        when(descriptor.getPlugin()).thenReturn(plugin);
        when(descriptor.getCompleteKey()).thenReturn(pluginKey + ":" + moduleKey);
        when(descriptor.getDependencies()).thenReturn(dependencies);
        List<ResourceDescriptor> resources = new ArrayList<>(resourceLocations.size());
        for (String location : resourceLocations) {
            ResourceDescriptor resource = mock(ResourceDescriptor.class);
            when(resource.getLocation()).thenReturn(location);
            resources.add(resource);
        }
        when(descriptor.getResourceDescriptors()).thenReturn(resources);
        moduleDescriptors.put(descriptor.getCompleteKey(), descriptor);
    }

    @Test
    public void testRecursiveDependencies() throws Exception {
        addModule("plugin", "module1", Arrays.asList("/file.soy", "/file.txt"), Arrays.asList("plugin:module2"));
        addModule("plugin", "module2", Arrays.asList("/file2.soy", "/file2.txt"), Arrays.asList("plugin:module1"));
        Set<URL> urls = factory.get("plugin:module1");
        assertNotNull(urls);
        assertEquals(2, urls.size());
        assertThat(urls, hasItem(new URL("file://plugin/file.soy")));
        assertThat(urls, hasItem(new URL("file://plugin/file2.soy")));
    }

    @Test
    public void testEmptyFileInDevMode() throws Exception {
        File soytest = File.createTempFile("soytest", ".soy");
        soytest.deleteOnExit();

        System.setProperty("atlassian.dev.mode", "true");
        // This is a bit of a hack, to make sure we're attempting to open the file and trigger the IOException
        addModule("", "module1", Arrays.asList(soytest.getAbsolutePath()), Collections.<String>emptyList());

        expectedException.expect(UncheckedExecutionException.class);
        expectedException.expectMessage("Empty file");
        Set<URL> urls = factory.get(":module1");
        assertNotNull(urls);
        assertEquals(2, urls.size());
        assertThat(urls, hasItem(new URL("file://plugin/file.soy")));
    }

    @Test
    public void testMissingFileInDevMode() throws Exception {
        File soytest = File.createTempFile("soytest", ".soy");

        System.setProperty("atlassian.dev.mode", "true");
        // This is a bit of a hack, to make sure we're attempting to open the file and trigger the IOException
        addModule("", "module1", Arrays.asList(soytest.getAbsolutePath()), Collections.<String>emptyList());

        soytest.delete();

        expectedException.expect(UncheckedExecutionException.class);
        expectedException.expectMessage("FileNotFoundException");
        Set<URL> urls = factory.get(":module1");
        assertNotNull(urls);
        assertEquals(2, urls.size());
        assertThat(urls, hasItem(new URL("file://plugin/file.soy")));
    }

    @After
    public void clearDevMode() {
        System.clearProperty("atlassian.dev.mode");
    }

}
