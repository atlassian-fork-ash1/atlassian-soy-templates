package com.atlassian.soy.impl.functions;

import com.atlassian.sal.api.message.HelpPath;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.soy.renderer.JsExpression;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HelpUrlFunctionTest {
    final String helpKey = "help.key";

    final String helpUrlStringValue = "https://www.atlassian.com/help";

    final String helpUrlAsJavaScriptString = "\"https:\\/\\/www.atlassian.com\\/help\"";

    @Mock
    HelpPath helpPath;

    @Mock
    HelpPathResolver helpPathResolver;

    HelpUrlFunction func;

    @Before
    public void setUp() {
        when(helpPath.getUrl()).thenReturn(helpUrlStringValue);
        when(helpPathResolver.getHelpPath(helpKey)).thenReturn(helpPath);

        func = new HelpUrlFunction(helpPathResolver);
    }

    @Test
    public void checkGenerate() {
        JsExpression expr = func.generate(new JsExpression("'" + helpKey + "'"));
        assertThat(expr.getText(), equalTo(helpUrlAsJavaScriptString));
    }

    @Test
    public void checkApply() {
        assertThat(func.apply(helpKey), equalTo(helpUrlStringValue));
    }
}
