package com.atlassian.soy.impl.functions;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.soy.renderer.StatefulSoyClientFunction;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyFunction;
import com.atlassian.soy.renderer.SoyFunctionModuleDescriptor;
import com.atlassian.soy.renderer.SoyServerFunction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.internal.stubbing.defaultanswers.ReturnsDeepStubs;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PluginSoyFunctionSupplierTest {
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private PluginEventManager pluginEventManager;

    private SoyFunctionModuleDescriptor pureFunctionDescriptor;
    private SoyFunctionModuleDescriptor statefulFunctionDescriptor;
    private SoyFunctionModuleDescriptor serverFunctionDescriptor;

    private PluginSoyFunctionSupplier supplier;

    @Before
    public void setUp() throws Exception {
        pureFunctionDescriptor = mockDescriptor(MyPureSoyFunction.class, "pure-plugin", "pure-function", "1.0.0");
        statefulFunctionDescriptor = mockDescriptor(MyStatefulSoyFunction.class, "stateful-plugin", "stateful-function", "2.0.0");
        serverFunctionDescriptor = mockDescriptor(MyPureServerOnlySoyFunction.class, "server-plugin", "server-function", "3.0.0");

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(SoyFunctionModuleDescriptor.class))
                .thenReturn(Arrays.asList(pureFunctionDescriptor, statefulFunctionDescriptor, serverFunctionDescriptor));

        supplier = new PluginSoyFunctionSupplier(pluginAccessor, pluginEventManager);
    }

    @Test
    public void testAddToUrl() throws Exception {
        supplier.registerListeners();
        verify(pluginEventManager).register(supplier);

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        supplier.addToUrl(urlBuilder);
        ArgumentCaptor<String> hashCaptor = ArgumentCaptor.forClass(String.class);
        verify(urlBuilder).addToHash(anyString(), hashCaptor.capture());

        String hash = hashCaptor.getValue();

        assertThat(hash, containsString("pure-plugin"));
        assertThat(hash, containsString("1.0.0"));
        assertThat(hash, not(containsString("pure-function")));

        assertThat(hash, containsString("stateful-plugin"));
        assertThat(hash, containsString("2.0.0"));
        assertThat(hash, not(containsString("stateful-function")));

        assertThat(hash, not(containsString("server-plugin")));
        assertThat(hash, not(containsString("3.0.0")));
        assertThat(hash, not(containsString("server-function")));

        verify(pureFunctionDescriptor, never()).getModule();
        verify(statefulFunctionDescriptor).getModule();
        StatefulSoyClientFunction statefulFunction = (StatefulSoyClientFunction) statefulFunctionDescriptor.getModule();
        verify(statefulFunction).addToUrl(same(urlBuilder));
        verify(serverFunctionDescriptor, never()).getModule();

        supplier.unregisterListeners();
        verify(pluginEventManager).unregister(supplier);
    }

    private SoyFunctionModuleDescriptor mockDescriptor(Class<? extends SoyFunction> implementationClass, String pluginKey, String moduleKey, String version) {
        SoyFunctionModuleDescriptor descriptor = mock(SoyFunctionModuleDescriptor.class, new ReturnsDeepStubs());

        when(descriptor.getCompleteKey()).thenReturn(pluginKey + ":" + moduleKey);
        when(descriptor.getKey()).thenReturn(moduleKey);

        when(descriptor.getPluginKey()).thenReturn(pluginKey);
        when(descriptor.getPlugin().getKey()).thenReturn(pluginKey);
        when(descriptor.getPlugin().getPluginInformation().getVersion()).thenReturn(version);

        SoyFunction function = mock(implementationClass);
        when(descriptor.getModule()).thenReturn(function);
        //noinspection unchecked
        when(descriptor.getModuleClass()).thenReturn((Class) implementationClass);

        return descriptor;
    }

    private interface MyPureSoyFunction extends SoyClientFunction {
    }

    private interface MyStatefulSoyFunction extends StatefulSoyClientFunction {
    }

    private interface MyPureServerOnlySoyFunction extends SoyServerFunction<Object> {
    }

}