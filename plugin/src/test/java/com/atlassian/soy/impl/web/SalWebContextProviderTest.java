package com.atlassian.soy.impl.web;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.message.LocaleResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SalWebContextProviderTest {
    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private LocaleResolver localeResolver;

    @InjectMocks
    private SalWebContextProvider webContextProvider;

    @Test
    public void testGetContextPathDelegatesToARequestAgnosticAPI() {
        final String expected = "/context";
        when(applicationProperties.getBaseUrl(UrlMode.RELATIVE)).thenReturn(expected);

        assertEquals(expected, webContextProvider.getContextPath());

        verify(applicationProperties).getBaseUrl(UrlMode.RELATIVE);
    }
}