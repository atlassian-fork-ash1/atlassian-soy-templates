package com.atlassian.soy.impl.webresource;

import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.soy.impl.functions.UrlEncodingSoyFunctionSupplier;
import com.atlassian.soy.impl.i18n.QueryParamsJsLocaleResolver;
import com.atlassian.soy.spi.web.WebContextProvider;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Locale;

import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@Ignore
public class SoyTransformerUrlBuilderTest {
    @Mock
    private UrlEncodingSoyFunctionSupplier soyFunctionSupplier;
    @Mock
    private WebContextProvider webContextProvider;

    private SoyTransformerUrlBuilder transformerUrlBuilder;

    @Before
    public void setUp() throws Exception {
        transformerUrlBuilder = new SoyTransformerUrlBuilder(soyFunctionSupplier, webContextProvider);
    }

    @Test
    public void testAddToUrl() throws Exception {
        when(webContextProvider.getLocale()).thenReturn(Locale.CHINESE);

        UrlBuilder urlBuilder = mock(UrlBuilder.class);

        transformerUrlBuilder.addToUrl(urlBuilder);
        verify(soyFunctionSupplier).addToUrl(same(urlBuilder));
        verify(urlBuilder).addToQueryString(QueryParamsJsLocaleResolver.QUERY_KEY, "zh");
    }
}
