package com.atlassian.soy.cli;

import com.atlassian.soy.impl.SoyManager;
import com.atlassian.soy.impl.SoyManagerBuilder;
import com.atlassian.soy.impl.functions.ServiceLoaderSoyFunctionSupplier;
import com.atlassian.soy.impl.functions.SoyProperties;
import com.atlassian.soy.impl.web.SimpleWebContextProvider;
import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;

/**
 * Compiles soy to js
 */
class SoyJsCompiler {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    void run(ClassLoader soyFunctionClassLoader, PathGlob i18nFileGlob, PathGlob source, String outputDirectory,
             String extension, String contextPath, boolean useAjsContextPath, boolean useAjsI18n) throws IOException {
        List<RelativePath> i18nPaths = FileFinder.findFiles(i18nFileGlob);

        Properties props = new Properties();
        props.setProperty(SoyProperties.USE_AJS_CONTEXT_PATH, Boolean.toString(useAjsContextPath));
        props.setProperty(SoyProperties.USE_AJS_I18N, Boolean.toString(useAjsI18n));

        SoyManagerBuilder builder = new SoyManagerBuilder()
                .i18nResolver(new FileBasedI18nResolver(i18nPaths))
                .functionSupplier(new ServiceLoaderSoyFunctionSupplier(soyFunctionClassLoader))
                .properties(props);

        if (contextPath != null) {
            builder = builder.webContextProvider(new SimpleWebContextProvider(contextPath));
        }

        SoyManager soyManager = builder.build();

        List<RelativePath> paths = FileFinder.findFiles(source);

        if (new File(outputDirectory).mkdirs()) {
            log.info("Created output directory: " + outputDirectory);
        }

        for (RelativePath path : paths) {
            final String compiledJs = soyManager.compile(Files.toString(path.absolutePath, Charset.defaultCharset()), path.absolutePath.getAbsolutePath());

            File outputFile = new File(outputDirectory, path.relativePath.replaceAll(".soy$", Matcher.quoteReplacement("." + extension)));
            if (!(outputFile.getParentFile().isDirectory() || outputFile.getParentFile().mkdirs())) {
                throw new IOException("Unable to create directory " + outputFile.getParentFile());
            }
            log.info("Writing compiled soy: " + outputFile);

            Files.write(compiledJs, outputFile, Charset.forName("UTF-8"));
        }
    }
}
