package com.atlassian.soy.cli;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;
import java.util.Collections;

import static org.junit.Assert.*;

public class FileBasedI18nResolverTest {
    @ClassRule
    public static TemporaryFolder tmpDir = new TemporaryFolder();

    private static File i18nFile;

    private FileBasedI18nResolver i18nResolver;

    @BeforeClass
    public static void writeI18nFiles() throws Exception {
        i18nFile = tmpDir.newFile();
        i18nFile.delete();
        Files.copy(FileBasedI18nResolverTest.class.getResourceAsStream("FileBasedI18nResolverTest.properties"), i18nFile.toPath());
    }

    @Before
    public void setUp() throws Exception {
        i18nResolver = new FileBasedI18nResolver(Collections.singletonList(new RelativePath(i18nFile, i18nFile.getName())));

    }

    @Test
    public void testGetTextResolvesMessage() throws Exception {
        assertEquals("Hello world", i18nResolver.getText("no.args.sanity"));
    }

    @Test
    public void testGetTextFormatsMessage() throws Exception {
        assertEquals("Hello {0} 'world'", i18nResolver.getText("no.args.escaping"));
    }
}