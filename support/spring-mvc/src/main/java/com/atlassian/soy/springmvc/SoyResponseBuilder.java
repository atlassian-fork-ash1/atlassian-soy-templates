package com.atlassian.soy.springmvc;

import org.springframework.web.servlet.View;

/**
 * Convenience builder for Soy ModelAndView. Handles errors neatly.
 *
 * @since 2.3
 */
public class SoyResponseBuilder extends AbstractSoyResponseBuilder<SoyResponseBuilder> {
    public SoyResponseBuilder(String viewName) {
        super(viewName);
    }

    public SoyResponseBuilder(View view) {
        super(view);
    }

    @Override
    protected SoyResponseBuilder self() {
        return this;
    }
}