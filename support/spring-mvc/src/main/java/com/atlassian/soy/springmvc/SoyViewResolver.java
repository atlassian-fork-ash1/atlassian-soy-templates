package com.atlassian.soy.springmvc;

import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of a Spring ViewResolver for Soy. Consumers will need this declared as a bean for Spring MVC to pick
 * it up.
 *
 * @since 2.3
 */
public class SoyViewResolver implements ViewResolver, Ordered {
    // redirect/forward prefixes (the same as UrlBasedViewResolver)
    public static final String REDIRECT_URL_PREFIX = "redirect:";
    public static final String FORWARD_URL_PREFIX = "forward:";

    protected final SoyTemplateRenderer templateRenderer;
    protected final InjectedDataFactory injectedDataFactory;

    private final int order;

    public SoyViewResolver(SoyTemplateRenderer templateRenderer, InjectedDataFactory injectedDataFactory) {
        this(templateRenderer, injectedDataFactory, 0);
    }

    public SoyViewResolver(SoyTemplateRenderer templateRenderer, InjectedDataFactory injectedDataFactory, int order) {
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.injectedDataFactory = checkNotNull(injectedDataFactory, "injectedDataFactory");
        this.order = order;
    }

    @Override
    public final View resolveViewName(final String viewName, final Locale locale) {
        // check for forward or redirect prefixes (based on UrlBasedViewResolver)
        if (viewName.startsWith(REDIRECT_URL_PREFIX)) {
            String redirectUrl = viewName.substring(REDIRECT_URL_PREFIX.length());
            return new RedirectView(redirectUrl, true, true);
        } else if (viewName.startsWith(FORWARD_URL_PREFIX)) {
            String forwardUrl = viewName.substring(FORWARD_URL_PREFIX.length());
            return new InternalResourceView(forwardUrl);
        }

        return constructSoyView(viewName);
    }

    protected SoyView constructSoyView(final String viewName) {
        return new SoyView(viewName, templateRenderer, injectedDataFactory);
    }

    @Override
    public int getOrder() {
        return order;
    }
}
