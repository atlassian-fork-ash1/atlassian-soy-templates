package com.atlassian.soy.springmvc;

import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.springframework.beans.factory.FactoryBean;

/**
 * FactoryBean for {@link SoyViewResolver}.
 * <p>
 * Example consumption:
 * <p>
 * in your web.xml:
 *
 *     <filter>
 *         <filter-name>webContextProvider</filter-name>
 *         <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
 *     </filter>
 *
 * and in your application's $appname-mvc.xml (or whatever you call it):
 *
 *     <bean id="soyViewResolver" class="com.atlassian.soy.spring.SoyTemplateRendererFactoryBean">
 *         <property name="soyTemplateRenderer" ref="soyTemplateRenderer"/>
 *     </bean>
 *
 * @since 2.4
 */
public class SoyViewResolverFactoryBean implements FactoryBean<SoyViewResolver> {

    private InjectedDataFactory injectedDataFactory;
    private SoyTemplateRenderer soyTemplateRenderer;

    public void setInjectedDataFactory(InjectedDataFactory injectedDataFactory) {
        this.injectedDataFactory = injectedDataFactory;
    }

    public void setSoyTemplateRenderer(SoyTemplateRenderer soyTemplateRenderer) {
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public SoyViewResolver getObject() throws Exception {
        if (injectedDataFactory == null) {
            setInjectedDataFactory(new EmptyInjectedDataFactory());
        }

        return new SoyViewResolver(soyTemplateRenderer, injectedDataFactory);
    }

    @Override
    public Class<SoyViewResolver> getObjectType() {
        return SoyViewResolver.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
