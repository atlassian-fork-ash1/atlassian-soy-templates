package com.atlassian.soy.springmvc.errors;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ErrorUtilsTest {

    @Test
    @SuppressWarnings("ThrowableInstanceNeverThrown")
    public void testGetLocalizedMessageWithNoNestedException() throws Exception {
        Exception e = new RuntimeException("errare humanum est");
        assertEquals("errare humanum est", ErrorUtils.getLocalizedMessageOfRootCause(e));
    }

    @Test
    @SuppressWarnings("ThrowableInstanceNeverThrown")
    public void testGetLocalizedMessageWhenNone() throws Exception {
        Exception e = new NullPointerException();
        assertEquals(e.toString(), ErrorUtils.getLocalizedMessageOfRootCause(e));
    }

    @Test
    public void testGetLocalizedMessageForNull() throws Exception {
        assertNull(ErrorUtils.getLocalizedMessageOfRootCause(null));
    }

}
