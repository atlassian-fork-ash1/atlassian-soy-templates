package com.atlassian.soy.springmvc.errors;

import org.junit.Test;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WebErrorUtilsTest {

    @Test
    public void testToFieldErrorMap() throws Exception {
        final FieldError fieldError1 = new FieldError("o", "field1", "message1");
        final FieldError fieldError2 = new FieldError("o", "field2", "message2");

        final Map<String, ? extends Iterable<String>> map = WebErrorUtils.toFieldErrorMap(asList(fieldError1, fieldError2));

        assertEquals(2, map.size());
        assertEquals(singletonList(fieldError1.getDefaultMessage()), map.get("field1"));
        assertEquals(singletonList(fieldError2.getDefaultMessage()), map.get("field2"));
    }

    @Test
    public void testToFormErrors() throws Exception {
        final ObjectError error1 = new ObjectError("o", "message1");
        final ObjectError error2 = new ObjectError("o", "message2");

        final List<String> formErrors = new ArrayList<>(WebErrorUtils.toFormErrors(asList(error1, error2)));

        assertEquals(2, formErrors.size());
        assertTrue(formErrors.contains("message1"));
        assertTrue(formErrors.contains("message2"));
    }

}
