package com.atlassian.soy.spring;

import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.web.WebContextProvider;
import org.springframework.context.MessageSource;

import java.io.Serializable;
import java.util.Locale;

/**
 * Simple Spring implementation which delegates to a MessageSource
 *
 * @since 2.3
 */
public class MessageSourceI18nResolver implements I18nResolver {

    public static final Serializable[] EMPTY_ARRAY = new Serializable[0];
    private final WebContextProvider webContextProvider;
    private final MessageSource messageSource;

    public MessageSourceI18nResolver(WebContextProvider webContextProvider, MessageSource messageSource) {
        this.webContextProvider = webContextProvider;
        this.messageSource = messageSource;
    }

    @Override
    public String getText(String key) {
        return getText(key, EMPTY_ARRAY);
    }

    @Override
    public String getText(String key, Serializable[] serializables) {
        return messageSource.getMessage(key, serializables, webContextProvider.getLocale());
    }

    @Override
    public String getText(Locale locale, String key) {
        return messageSource.getMessage(key, EMPTY_ARRAY, locale);
    }

    @Override
    public String getRawText(Locale locale, String key) {
        return getText(locale, key);
    }
}
