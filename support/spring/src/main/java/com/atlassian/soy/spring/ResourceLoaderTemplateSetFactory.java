package com.atlassian.soy.spring;


import com.atlassian.soy.spi.TemplateSetFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Simple template set factory that searches for templates
 *
 * @since 2.3
 */
public class ResourceLoaderTemplateSetFactory implements TemplateSetFactory, ResourceLoaderAware {

    public static final String DEFAULT_FILE_PATTERN = "classpath:view/**/*.soy";

    private static final Logger log = LoggerFactory.getLogger(ResourceLoaderTemplateSetFactory.class);

    private final Iterable<String> filePatterns;

    private ResourceLoader resourceLoader;

    public ResourceLoaderTemplateSetFactory() {
        this(Collections.singleton(DEFAULT_FILE_PATTERN));
    }

    public ResourceLoaderTemplateSetFactory(Iterable<String> filePatterns) {
        this.filePatterns = filePatterns;
    }

    @Override
    public void clear() {

    }

    @Override
    public Set<URL> get(String ignored) {
        // completeModuleKey will always be "static" in this case (as we're not pluggable) so ignore it.

        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver(resourceLoader);
        Set<URL> files = new HashSet<>();

        for (String filePattern : filePatterns) {
            try {
                for (Resource resource : patternResolver.getResources(filePattern)) {
                    files.add(resource.getURL());
                }
            } catch (IOException e) {
                log.warn("Failed to scan resource loader {}", filePattern, e);
            }
        }

        return files;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
