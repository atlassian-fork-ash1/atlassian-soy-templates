package com.atlassian.soy.spring;

import com.atlassian.soy.spi.web.WebContextProvider;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

/**
 * Spring specific implementation of WebContextProvider. When registered as a servlet filter it stores request & response
 * in thread locals and implements the SPI using these.
 *
 * @since 2.3
 * @deprecated in 4.2.0 for removal in 5.0 use {@link SpringWebContextProvider} instead in conjunction with
 *              {@code org.springframework.web.context.request.RequestContextListener},
 *              {@code org.springframework.web.filter.RequestContextFilter} or
 *              {@code org.springframework.web.servlet.FrameworkServlet}
 */
@Deprecated
public class WebContextProviderServletFilter extends OncePerRequestFilter implements WebContextProvider {
    private final ThreadLocal<HttpServletRequest> request = new ThreadLocal<>();
    private final ThreadLocal<HttpServletResponse> response = new ThreadLocal<>();

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        setRequest(httpServletRequest);
        setResponse(httpServletResponse);
        try {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } finally {
            clearRequest();
            clearResponse();
        }
    }

    public HttpServletRequest getRequest() {
        return request.get();
    }

    public void clearRequest() {
        this.request.remove();
    }

    void setRequest(HttpServletRequest httpServletRequest) {
        this.request.set(httpServletRequest);
    }

    public HttpServletResponse getResponse() {
        return response.get();
    }

    public void clearResponse() {
        this.response.remove();
    }

    void setResponse(HttpServletResponse httpServletResponse) {
        this.response.set(httpServletResponse);
    }

    @Override
    public String getContextPath() {
        return getRequest().getContextPath();
    }

    @Override
    public Locale getLocale() {
        return getResponse().getLocale();
    }
}
